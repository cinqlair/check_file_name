#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <iostream>

bool updateName(QString *newName, bool upper2lower=false)
{
    bool nameChanged=false;
    // Check for spaces
    if (newName->contains(QString(" ")))
    {
        *newName=newName->replace(QString(" "),QString("_"));
        nameChanged=true;
    }

    // Check for accented caracters
    if (newName->contains(QString("é")))
    {
        *newName=newName->replace(QString("é"),QString("e"));
        nameChanged=true;
    }

    // Check for accented caracters
    if (newName->contains(QString("è")))
    {
        *newName=newName->replace(QString("è"),QString("e"));
        nameChanged=true;
    }

    // Check for accented caracters
    if (newName->contains(QString("ë")))
    {
        *newName=newName->replace(QString("ë"),QString("e"));
        nameChanged=true;
    }

    // Check for accented caracters
    if (newName->contains(QString("ê")))
    {
        *newName=newName->replace(QString("ê"),QString("e"));
        nameChanged=true;
    }

    // Check for accented caracters
    if (newName->contains(QString("à")))
    {
        *newName=newName->replace(QString("à"),QString("a"));
        nameChanged=true;
    }

    // Check for apostroph
    if (newName->contains(QString("'")))
    {
        *newName=newName->replace(QString("'"),QString("_"));
        nameChanged=true;
    }

    // Check for apostroph
    if (newName->contains(QString("(")))
    {
        *newName=newName->replace(QString("("),QString("_"));
        nameChanged=true;
    }

    // Check for apostroph
    if (newName->contains(QString(")")))
    {
        *newName=newName->replace(QString(")"),QString("_"));
        nameChanged=true;
    }


    // Check for apostroph
    if (newName->contains(QString("Ú")))
    {
        *newName=newName->replace(QString("Ú"),QString("e"));
        nameChanged=true;
    }


    if (upper2lower)
    {
        // Check for uppercase characters
        for (char ch='A';ch<='Z';ch++)
        {
            if (newName->contains(QString(ch)))
            {
                *newName=newName->replace(QString(ch),QString(ch-'A'+'a'));
                nameChanged=true;
            }
        }
    }

    // Return true if the filename has changed
    return nameChanged;
}


bool parseDirectories()
{
    // For each sub folder
    QDirIterator itDir(QDir().currentPath(), QDir::Dirs |  QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
    while (itDir.hasNext())
    {
        itDir.next();
        // Check the filename
        QString newName=itDir.fileName();
        if (updateName(&newName))
        {
            // Get path without the filename
            QDir dir(itDir.filePath());
            dir.cdUp();

            // Rename file
            if (dir.rename(itDir.filePath(),dir.absolutePath()+QString("/")+newName))

                // Success
                std::cout << "Rename \"" << itDir.filePath().toStdString() << "\" -> \"" << (dir.absolutePath()+QString("/")+newName).toStdString() << "\"" << std::endl;
            else
            {

                // Error while renaming
                std::cerr << std::endl;
                std::cerr << "\033[1;31mFatal error while renaming\033[0m \"" << itDir.filePath().toStdString() << "\" \033[1;31mto\033[0m \"" << (dir.absolutePath()+QString("/")+newName).toStdString() << "\"" << std::endl;
                exit (0);
            }
            return true;
        }
    }
    // No directory has been updated
    return false;
}

bool parseFiles()
{
    // For each sub folder
    QDirIterator itFile(QDir().currentPath(), QDir::Files, QDirIterator::Subdirectories);
    while (itFile.hasNext())
    {
        itFile.next();
        // Check the filename
        QString newName=itFile.fileName();
        if (updateName(&newName))
        {
            // Get path without the filename
            QDir dir(itFile.filePath());
            dir.cdUp();

            //qDebug() << itFile.filePath() << dir.absolutePath()+QString("/")+newName;

            // Rename file
            if (dir.rename(itFile.filePath(),dir.absolutePath()+QString("/")+newName))

                // Success
                std::cout << "Rename \"" << itFile.filePath().toStdString() << "\" -> \"" << (dir.absolutePath()+QString("/")+newName).toStdString() << "\"" << std::endl;
            else
            {

                // Error while renaming
                std::cerr << std::endl;
                std::cerr << "\033[1;31mFatal error while renaming\033[0m \"" << itFile.filePath().toStdString() << "\" \033[1;31mto\033[0m \"" << (dir.absolutePath()+QString("/")+newName).toStdString() << "\"" << std::endl;
                exit (0);
            }

            return true;
        }
    }
    // No directory has been updated
    return false;
}


bool checkFileName(QString name)
{
    int numberValid=0;
    numberValid+=name.count(".");
    numberValid+=name.count("_");
    numberValid+=name.count("-");
    for (char ch='a';ch<='z';ch++) numberValid+=name.count(ch);
    for (char ch='A';ch<='Z';ch++) numberValid+=name.count(ch);
    for (char ch='0';ch<='9';ch++) numberValid+=name.count(ch);


    return numberValid==name.length();
}


void searchForbiddenCharacters()
{
    int counter=0;

    // For each sub folder
    QDirIterator itName(QDir().currentPath(), QDir::AllEntries |  QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
    while (itName.hasNext())
    {
        itName.next();
        /*
        if (itName.fileInfo().isDir())
        {
            std::cout << "Parse directory " << itName.filePath().toStdString() << std::endl;
        }
        */

        if (!checkFileName(itName.fileName()))
        {
            std::cout << "\033[1;35mForbidden character in \033[0m \"" << itName.filePath().toStdString() << std::endl;
            counter++;
        }
    }
    std::cout << counter << " invalid file name(s)" << std::endl;
}


int main(int argc, char *argv[])
{

    std::cout << std::endl;
    std::cout << "::: PARSE DIRECTORIES :::" << std::endl;
    std::cout << std::endl;

    // Parse directories
    while (parseDirectories());

    std::cout << std::endl;
    std::cout << "::: PARSE FILES :::" << std::endl;
    std::cout << std::endl;

    // Parse files
    while (parseFiles());

    std::cout << std::endl;
    std::cout << "::: CHECK FILE NAME :::" << std::endl;
    std::cout << std::endl;

    searchForbiddenCharacters();

    return 0;
}
